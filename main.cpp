//1. Хеш-таблица
//Программа должна вывести для каждой операции одну из двух строк OK или FAIL.
//Для операции '?': OK, если элемент присутствует во множестве. FAIL иначе.
//Для операции '+': FAIL, если добавляемый элемент уже присутствует во множестве и потому не может быть добавлен. OK иначе.
//Для операции '-': OK, если элемент присутствовал во множестве и успешно удален. FAIL иначе.

#include <iostream>
#include "vector"
#include "assert.h"
#include "fstream"

#define LOAD_FACTOR 0.75

using  std::string;
using std::cin;
using std::cout;
using std::vector;
using std::endl;
using std::ifstream;
using std::ofstream;

template < class T, class H >
class HashTable
{
public:
    explicit HashTable( const H & hasher );
    HashTable( const HashTable & ) = delete;
    HashTable& operator = ( const HashTable & ) = delete;
    ~HashTable();

    bool add( const T & value );
    bool del( const T & value );
    bool has( const T & value ) const;
private:
    void grow();
    enum fieldStatus
    {
        BUSY,
        EMPTY,
        DELETED
    };
    struct HashNode
    {
        T value;
        fieldStatus status;
        explicit HashNode( const T& value ): value( value ), status( BUSY ){};
        explicit HashNode(): status( EMPTY ){};
    };
    vector<HashNode> table;
    int countValues;
    H hasher;
};

template < class T, class H >
void HashTable<T, H>::grow()
{
    vector<HashNode> oldTable = table;
    table = vector<HashNode> (table.size() * 2 );
    countValues = 0;
    for( int i = 0; i < oldTable.size(); ++i )
        if( oldTable[i].status == BUSY )
            this->add(oldTable[i].value);
}

template < class T, class H >
HashTable< T, H >::HashTable( const H &hasher )
        :hasher( hasher ), table( 8),
         countValues(0)
{
}

template < class T, class H>
HashTable< T, H >::~HashTable()
{
}

template < class T, class H>
bool HashTable< T, H >::add( const T &value )
{
    if( table.size() * LOAD_FACTOR < countValues )
        grow();

    int hash = hasher( value ) % table.size();
    int firstDelPos = -1;
    int stepCount = 0;
    while ( table[hash].status !=  EMPTY && stepCount++ < table.size() )
    {
        if( table[hash].status == DELETED && firstDelPos == -1 )
            firstDelPos = hash;
        if( table[hash].status == BUSY && table[hash].value == value )
            return false;
        hash = hasher( hash, stepCount ) % table.size() ;
    }

    if( table[hash].status ==  EMPTY )
    {
        if( firstDelPos == -1 )
        {
            table[hash].value = value;
            table[hash].status = BUSY;
            ++countValues;
        }
        else
        {
            table[firstDelPos].value = value;
            table[firstDelPos].status = BUSY;
        }
    }
    else
    {
        assert( firstDelPos != -1 );
        table[firstDelPos].value = value;
        table[firstDelPos].status = BUSY;
    }

    return true;
}

template < class T, class H>
bool HashTable< T, H >::has( const T &value ) const
{
    int hash = hasher( value ) % table.size();
    int stepCount = 0;
    while ( table[hash].status != EMPTY && stepCount++ < table.size() )
    {
        if(  table[hash].status == BUSY && table[hash].value == value )
            return true;
        hash = hasher( hash, stepCount ) % table.size();
    }
    return false;
}

template < class T, class H>
bool HashTable< T, H >::del( const T &value )
{
    int hash = hasher( value ) % table.size();
    int stepCount = 0;
    while ( table[hash].status != EMPTY && stepCount++ < table.size() )
    {
        if( table[hash].status == BUSY && table[hash].value == value )
        {
            table[hash].status = DELETED;
            --countValues;
            return true;
        }
        hash = hasher( hash, stepCount ) % table.size();
    }
    return false;
}

struct Hasher
{
    unsigned int operator() ( const string& table ) const //вычисление хэша 1 раз
    {
        unsigned int hash = 0;
        for( int i = 0; i < table.size(); ++i )
        {
            hash = hash * 7 + table[i];
        }
        return  hash;
    }
    unsigned int operator() ( int lastHash, int tryCount ) const //вычисление хэша на tryCount раз
    {
        return lastHash + tryCount % 128;
    }

};

int main() {
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    if( !fin.is_open() || !fout.is_open() )
        return -1;
    Hasher hasher;
    HashTable< string, Hasher > table( hasher );
    char operation;
    string value;
    while ( fin >> operation >> value )
    {
        switch (operation)
        {
            case '+':
                fout << ( table.add(value) ? "OK" : "FAIL" ) << endl;
                break;
            case '?':
                fout << ( table.has(value) ? "OK" : "FAIL" ) << endl;
                break;
            case '-':
                fout << ( table.del(value) ? "OK" : "FAIL" ) << endl;
                break;
        }
    }
    fin.close();
    fout.close();
    return 0;
}
